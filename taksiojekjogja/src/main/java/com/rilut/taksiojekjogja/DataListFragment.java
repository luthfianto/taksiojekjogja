package com.rilut.taksiojekjogja;

import com.rilut.taksiojekjogja.R;

import android.app.ListFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class DataListFragment extends ListFragment {
    ListView l=null;
    final String[]
        TAXI_NAMES={"ASA Taxi", "Armada Taxi", "Centris Taxi", "Gunungjati Taksi", "JAS Taxi (Gembiraloka Zoo)","Pamungkas Taksi", "Pendawa Taxi", "Pataga Taxi", "Progo Taxi", "Rajawali Taxi", "Ria Taksi", "Sadewo Taksi", "Serasi Autoraya", "Setia Kawan Taksi", "Tambayo Taxi", "Vetri Taksi Puskoveri DIY"},
        TAXI_ADDRS={"Jl. Tanjung Baru 5", "Jl. Pingit Kidul 12", "Jl. Ringroad Utara Ngemplak Nganti", "Perum Soko Asri BI H/6 Kls", "Jl Kapten Tendean","Jl. Ringroad Barat 85", "Jl. SWK 102", "Jl. Pramuka 9B", "Jl. Ringroad Barat 85", "Jl. AM. Sangaji 16", "Jl Ringroad Barat 85", "Jl. Prawirotaman MG 10W", "Jl. Magelang Km 7,2 Smn", "Jl. Ringroad Sh 66", "Jl. Pingit Kidul 12", "Jl Tentara Rakyat Mataram 6"},
        TAXI_TELS ={"0274545545", "0274517248", "0274544977", "0274496810", "0274373737", "0274621333", "0274447231", "0274389234", "0274621333", "0274512976", "0274621333", "0274382262", "0274864567", "0274522333", "0274512787", "0274563551"},
        OJEK_NAMES={"O'Jack","Prima"},
        OJEK_ADDRS={"Jl Sarjito 11","Jembatan Monjali"},
        OJEK_TELS={"02747000707","02749252525"}
    ;
    String[] names, addrs, tels;
    public DataListFragment(){}
    public DataListFragment(int tabs){
        if(tabs==0){
            names=OJEK_NAMES;
            addrs=OJEK_ADDRS;
            tels=OJEK_TELS;
        } else {
            names=TAXI_NAMES;
            addrs=TAXI_ADDRS;
            tels=TAXI_TELS;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_2, android.R.id.text1, this.names) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                text1.setText(names[position]);
                text2.setText(addrs[position]);
                return view;
            }
        };
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onListItemClick (ListView l, View v, int position, long id)
    {
        Intent intent= new Intent("android.intent.action.CALL");
        intent.setData(Uri.parse("tel:" + tels[position]));
        startActivity(intent);
    }
}